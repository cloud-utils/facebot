# Setup
- Export requirements
```shell
pdm export -f requirements --without-hashes > requirements.txt
```
- Set function handler to `src.faces.main.handler`
- Set environment variables
  - aws_endpoint_url
  - aws_access_key_id
  - aws_region_name
  - aws_secret_access_key
  - aws_bucket_name
  - tg_token
  - tg_user_id
- Add sqs trigger for this function
