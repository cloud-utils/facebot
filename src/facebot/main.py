import io

import aioboto3
from aiogram import Bot
from aiogram import Dispatcher

from .base import Context
from .base import Event
from .base import EventMessage
from .base import Response
from .settings import Settings
from .tg import process_event
from .tg import register_handlers
from .tg import send_face


async def handler(event: Event, context: Context):
    settings = Settings()
    boto_session = aioboto3.Session(
        aws_access_key_id=settings.aws_access_key_id,
        aws_secret_access_key=settings.aws_secret_access_key,
        region_name=settings.aws_region_name,
    )

    bot = Bot(settings.tg_token)
    dp = Dispatcher(bot)
    if event.get('body'):
        await register_handlers(dp)
        await process_event(event, dp)

    messages = event.get('messages')
    if messages:
        msg = EventMessage.parse_obj(messages[0])
        files = msg.details.message.body.split()

        async with boto_session.client(
            's3',
            endpoint_url=settings.aws_endpoint_url,
        ) as s3client:
            for f in files:
                fileobj = io.BytesIO()
                await s3client.download_fileobj(
                    settings.aws_bucket_name,
                    f,
                    fileobj,
                )
                fileobj.name = f
                fileobj.seek(0)
                await send_face(bot, settings.tg_user_id, fileobj)

    return Response(
        statusCode=200,
        body='Done',
    ).dict(by_alias=True, exclude_unset=True)
