import datetime
import typing as t

from pydantic import BaseModel


def to_camel(string: str) -> str:
    words = string.split('_')
    return words[0] + ''.join(word.title() for word in words[1:])


def to_pascal(string: str) -> str:
    words = string.split('_')
    return ''.join(word.title() for word in words)


class Attributes(BaseModel):
    approximate_first_receive_timestamp: str
    approximate_receive_count: str
    sent_timestamp: str

    class Config:
        alias_generator = to_pascal


class Message(BaseModel):
    message_id: str
    md5_of_body: str
    body: str
    attributes: Attributes
    message_attributes: t.Optional[dict[str, t.Any]]
    md5_of_message_attributes: t.Optional[str]


class Details(BaseModel):
    queue_id: str
    message: Message


class TracingContext(BaseModel):
    trace_id: str
    spand_id: t.Optional[str]
    parent_span_id: t.Optional[str]


class EventMetadata(BaseModel):
    event_id: str
    event_type: str
    created_at: datetime.datetime
    tracing_context: t.Optional[TracingContext]
    cloud_id: str
    folder_id: str


class EventMessage(BaseModel):
    event_metadata: EventMetadata
    details: Details


class Event(t.TypedDict, total=False):
    messages: list[EventMessage]
    body: str


class IAMToken(t.TypedDict):
    access_token: str
    expires_in: int
    token_type: str


class Context(t.NamedTuple):
    token: IAMToken


class Response(BaseModel):
    status_code: int
    headers: t.Optional[dict[str, str]]
    multi_value_headers: t.Optional[dict[str, str]]
    body: str
    is_base64_encoded: bool = False

    class Config:
        alias_generator = to_camel
