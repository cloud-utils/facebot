import io
import os
import typing as t

import aioboto3

from .keygen import get_origin
from .settings import Settings


async def set_name_to_img(
    user_name: str,
    filename: str,
    session: t.Optional[aioboto3.Session] = None,
    settings: t.Optional[Settings] = None,
) -> None:
    if settings is None:
        settings = Settings()
    if session is None:
        session = aioboto3.Session(
            aws_access_key_id=settings.aws_access_key_id,
            aws_secret_access_key=settings.aws_secret_access_key,
            region_name=settings.aws_region_name,
        )

    async with session.client(
        's3',
        endpoint_url=settings.aws_endpoint_url,
    ) as s3client:
        resp = await s3client.head_object(
            Bucket=settings.aws_bucket_name,
            Key=filename,
        )
        metadata = resp['Metadata']
        names = metadata.setdefault('names', '')
        names += f' {user_name}'
        metadata['names'] = names
        await s3client.copy_object(
            Bucket=settings.aws_bucket_name,
            Key=filename,
            CopySource=os.path.join(settings.aws_bucket_name, filename),
            Metadata=metadata,
            MetadataDirective='REPLACE',
        )


async def get_img_by_name(
    user_name: str,
    session: t.Optional[aioboto3.Session] = None,
    settings: t.Optional[Settings] = None,
) -> list[io.BytesIO]:
    if settings is None:
        settings = Settings()
    if session is None:
        session = aioboto3.Session(
            aws_access_key_id=settings.aws_access_key_id,
            aws_secret_access_key=settings.aws_secret_access_key,
            region_name=settings.aws_region_name,
        )
    images = []
    async with session.client(
        's3',
        endpoint_url=settings.aws_endpoint_url,
    ) as client:
        resp = await client.list_objects(
            Bucket=settings.aws_bucket_name,
        )
        files = resp['Contents']
        for f in files:
            key = get_origin(f['Key'])
            if not key:
                continue
            resp = await client.head_object(
                Bucket=settings.aws_bucket_name,
                Key=f['Key'],
            )
            names = resp['Metadata'].get('names')
            if names and user_name in names.split():
                fileobj = io.BytesIO()
                await client.download_fileobj(
                    settings.aws_bucket_name,
                    key,
                    fileobj,
                )
                fileobj.seek(0)
                images.append(fileobj)

    return images
