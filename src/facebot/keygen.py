import base64
import os
import typing as t


def generate_id(key: str) -> str:
    return base64.b64encode(key.encode()).decode()[:5]


def get_origin(filename: str) -> t.Optional[str]:
    name, ext = os.path.splitext(filename)
    origin = name.split('_')[0]
    id = name.split('_')[-1]
    if origin == id:
        return None
    origin += ext
    if generate_id(origin) == id:
        return origin
    else:
        return None
