import io
import json
import typing as t

from aiogram import Bot
from aiogram import Dispatcher
from aiogram.types import InputFile
from aiogram.types import MediaGroup
from aiogram.types import Message
from aiogram.types import Update

from .base import Event
from .s3 import get_img_by_name
from .s3 import set_name_to_img


async def send_face(
    bot: Bot,
    chat_id: t.Union[int, str],
    fileobj: io.BytesIO,
):
    await bot.send_photo(chat_id, fileobj, caption=fileobj.name)


async def process_event(event: Event, dp: Dispatcher):
    update = json.loads(event['body'])
    Bot.set_current(dp.bot)
    update = Update.to_object(update)
    await dp.process_update(update)


async def register_handlers(dp: Dispatcher):
    dp.register_message_handler(find_name, commands=['find'])
    dp.register_message_handler(set_name, commands=['set'])


async def find_name(message: Message):
    name = message.get_args()
    photos = await get_img_by_name(name)
    media = MediaGroup()
    if not photos:
        await message.reply(f'No images with {name}')
    for photo in photos:
        media.attach_photo(InputFile(photo))
    await message.reply_media_group(media)


async def set_name(message: Message):
    name, filename = message.get_args().split()
    await set_name_to_img(name, filename)
    await message.answer(f"Ok, it's {name}")
